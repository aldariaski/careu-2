[![pipeline status](https://gitlab.com/aldariaski/careu-2/badges/master/pipeline.svg)](https://gitlab.com/aldariaski/careu-2/-/commits/master) [![coverage report](https://gitlab.com/aldariaski/careu-2/badges/master/coverage.svg)](https://gitlab.com/aldariaski/careu-2/-/commits/master)

## Anggota Kelompok

    1. Muhamad Fauzi Ridwan
    2. Kirene Naomi Kristin Pangaribuan
    3. Yusuf Fakhri Aldrian
    4. Jesika Avonia Hutauruk

## Link Herokuapp: 
https://careu.herokuapp.com

## Cerita Aplikasi CareU. 
adalah sebuah aplikasi yang bertujuan untuk membantu orang lain yang sedang mengalami kesehatan mental yang buruk dalam menghadapi pandemi COVID-19. Karena pada umumnya, orang yang sedang mengalami masalah mental tidak mengetahui betul terkait gejala yang timbul. Sehingga CareU membantu dengan menyediakan informasi terkait gejala-gejala kesehatan mental yang buruk seperti stress, insomnia, bipolar disorder dan lainnya. Selain itu terdapat Quarantine Journal dimana user dapat bercerita apapun tentang perasaan yang sedang dialaminya. Hal ini akan sangat membantu untuk memulihkan kesehatan mentalnya menjadi lebih baik lagi.

Daftar Fitur

    1. Quarantine Journal (jurnal)
    2. Chat us (bercerita kepada tim CareU)
    3. InforCare (informasi terkait gejala kesehatan mental)
